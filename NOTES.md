# Steps for the Machine Learning 3rd assignment:

* Feature Extraction - given in the python script by the professor
* Perform the classification with traditional classifiers
* Perform deep learning classification
* Compare both methods (in terms of measurements and runtimes)
    * Create confusion matrix (note: if ImageAI does not support it, lets automate that shit somehow)
* Car dataset and fruit dataset
    * Car is binary classification
    * Fruit has 30 classes

# What to use:
* Car classification: https://github.com/tuwien-musicir/DL_Tutorial/blob/master/Car_recognition.ipynb
* Deep learning network: https://github.com/OlafenwaMoses/ImageAI and https://github.com/ilirosmanaj/detect_kermit
    * We should probably just modify the detect_kermit

# How work is split:
* Fruit dataset: Gent
* Car classification: Ilir
