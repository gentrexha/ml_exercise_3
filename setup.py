from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Assignment 3 for Lecture Machine Learning. TU Wien. February 2019',
    author='Gent Rexha Ilir Osmanaj',
    license='',
)
