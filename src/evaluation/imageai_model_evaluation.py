from os.path import isfile

import click
import os

from imageai.Prediction.Custom import CustomImagePrediction

EXECUTION_PATH = os.getcwd()
BASE_IMAGE_PATH = '../../data/raw/{dataset}'


def predict_image(image_name: str, model: CustomImagePrediction) -> dict:
    """Predicts a given image with the supplied prediction model"""
    print('\nPredicting the {} image'.format(image_name))

    predictions, probabilities = model.predictImage(os.path.join(EXECUTION_PATH, image_name), result_count=2)

    representation = {}
    for eachPrediction, eachProbability in zip(predictions, probabilities):
       representation[eachPrediction]= '{0:.2f}%'.format(float(eachProbability))

    return representation


@click.command()
@click.option('-dataset', '-d', type=click.Choice(['fruit', 'car']), default='car')
@click.option('-filenames', '-f', help="Filename of the image to predict in. Also comma seperated allowed",
              required=True)
@click.option('--no_of_objects', '-n', type=int, required=True, default=2)
def main(dataset:str, filenames: str, number_of_objects: int):
    data_directory = BASE_IMAGE_PATH.format(dataset=dataset)

    model_directory = '{}/models/model.h5'.format(data_directory)
    model_class_directory = '{}/json/model_class.json'.format(data_directory)

    if not (isfile(model_directory) and isfile(model_class_directory)):
        raise ValueError('Invalid data path given')
    
    model = CustomImagePrediction()
    model.setModelTypeAsResNet()

    model.setModelPath(model_directory)
    model.setJsonPath(model_class_directory)
    model.loadModel(num_objects=number_of_objects)  # number of objects on your trained model

    for image in filenames.split(','):
        print(predict_image(image_name=image, model=model))


if __name__ == '__main__':
    main()
