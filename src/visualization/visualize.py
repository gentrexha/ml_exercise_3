# import the necessary packages
import sys
from matplotlib import pyplot as plt
# import libraries for image feature extraction
import numpy as np
import cv2
from PIL import Image
from pathlib import Path


def featureExtractionVisualizationFruit():
    # Before we do actual feature extaction just for curiosity, let's look at one image,
    # to illustrate what we are going to do
    # For OpenCV (need Version 2.4+) for Python 2.7, on Ubuntu Linux, just "sudo apt install python-opencv"
    # In other OS, that might be different.
    # demoImageName = fileNames[1]
    # demoImage = imagePath + demoImageName
    p = Path('C:/Projects/University/Machine Learning/ml_exercise_3/data/raw/Fruit/FIDS30/acerolas/5.jpg')
    demoImage = str(p)
    demoImageName = 'acerolas/1.jpg'

    print("Showing demo feature extraction on image {}".format(demoImage))

    # load the image & plot it
    imagePIL = Image.open(demoImage)
    imgplot = plt.imshow(imagePIL)
    plt.title(demoImageName)

    # now we compute a colour histogram using the histogram function in pillow
    # This gives us one histogram with 768 values, which is 3 x 256 values for each colour
    # For each colour channel, each value repesent the count how many pixels have that colour intensity
    featureVector = imagePIL.histogram()

    # We plot this histogram
    plt.figure()
    plt.plot(featureVector[:256], 'r')
    plt.plot(featureVector[257:512], 'g')
    plt.plot(featureVector[513:], 'b')
    plt.xlim([0, 256])
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    plt.title("Colour Histogram, using PIL")

    # plt.show()

    # An alternative is to use open CV
    imageOpenCV = cv2.imread(demoImage)

    # OpenCV is a bit weird, because it changes the channel order, it stores them as BGR, instead of RGB
    # So if we want to display the image, we have to invert it
    plt.figure()
    plt.imshow(cv2.cvtColor(imageOpenCV, cv2.COLOR_BGR2RGB))

    chans = cv2.split(
        imageOpenCV)  # split the image in the different channels (RGB, but in open CV, it is BGR, actually..)
    colors = ("b", "g", "r")
    plt.figure()
    plt.title("Colour Histogram, using OpenCV")
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    featuresOpenCV = []

    # loop over the image channels
    for (chan, color) in zip(chans, colors):
        # create a histogram for the current channel and add it to the resulting histograms array (of arrays)
        # We can specifiy here in the 4th argument how many bins we want -
        # 256 means the same as in the previous histogram
        histOpenCV = cv2.calcHist([chan], [0], None, [256], [0, 256])
        featuresOpenCV.extend(histOpenCV)

        # plot the histogram of the current colour
        plt.plot(histOpenCV, color=color)
        plt.xlim([0, 256])

    # Now we have a 2D-array - 256 values for each of 3 colour channels.
    # To input this into our machine learning, we need to "flatten" the features into one larger 1D array
    # the size of this will be 3 x 256 = 768 values
    featureVectorOpenCV = np.array(featuresOpenCV).flatten()

    # show all the plots
    plt.show()


def featureExtractionVisualizationCar():
    # Before we do actual feature extaction just for curiosity, let's look at one image,
    # to illustrate what we are going to do
    # For OpenCV (need Version 2.4+) for Python 2.7, on Ubuntu Linux, just "sudo apt install python-opencv"
    # In other OS, that might be different.
    # demoImageName = fileNames[1]
    # demoImage = imagePath + demoImageName
    p = Path('C:/Projects/University/Machine Learning/ml_exercise_3/data/raw/CarData/TrainImages/neg-0.pgm')
    demoImage = str(p)
    demoImageName = 'neg-0.pgm'

    print("Showing demo feature extraction on image {}".format(demoImage))

    # load the image & plot it
    imagePIL = Image.open(demoImage)
    imgplot = plt.imshow(imagePIL)
    plt.title(demoImageName)

    # now we compute a colour histogram using the histogram function in pillow
    # This gives us one histogram with 768 values, which is 3 x 256 values for each colour
    # For each colour channel, each value repesent the count how many pixels have that colour intensity
    featureVector = imagePIL.histogram()

    # We plot this histogram
    plt.figure()
    plt.plot(featureVector[:256], 'r')
    plt.plot(featureVector[257:512], 'g')
    plt.plot(featureVector[513:], 'b')
    plt.xlim([0, 256])
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    plt.title("Colour Histogram, using PIL")

    # plt.show()

    # An alternative is to use open CV
    imageOpenCV = cv2.imread(demoImage)

    # OpenCV is a bit weird, because it changes the channel order, it stores them as BGR, instead of RGB
    # So if we want to display the image, we have to invert it
    plt.figure()
    plt.imshow(cv2.cvtColor(imageOpenCV, cv2.COLOR_BGR2RGB))

    chans = cv2.split(
        imageOpenCV)  # split the image in the different channels (RGB, but in open CV, it is BGR, actually..)
    colors = ("b", "g", "r")
    plt.figure()
    plt.title("Colour Histogram, using OpenCV")
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    featuresOpenCV = []

    # loop over the image channels
    for (chan, color) in zip(chans, colors):
        # create a histogram for the current channel and add it to the resulting histograms array (of arrays)
        # We can specifiy here in the 4th argument how many bins we want -
        # 256 means the same as in the previous histogram
        histOpenCV = cv2.calcHist([chan], [0], None, [256], [0, 256])
        featuresOpenCV.extend(histOpenCV)

        # plot the histogram of the current colour
        plt.plot(histOpenCV, color=color)
        plt.xlim([0, 256])

    # Now we have a 2D-array - 256 values for each of 3 colour channels.
    # To input this into our machine learning, we need to "flatten" the features into one larger 1D array
    # the size of this will be 3 x 256 = 768 values
    featureVectorOpenCV = np.array(featuresOpenCV).flatten()

    # show all the plots
    plt.show()


def main():
    """
    Main function.
    :return:
    """
    # featureExtractionVisualizationFruit()
    featureExtractionVisualizationCar()


if __name__ == '__main__':
    main()
    sys.exit(0)

