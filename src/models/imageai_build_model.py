import click

from os.path import isdir
from imageai.Prediction.Custom import ModelTraining

BASE_IMAGE_PATH = '../../data/raw/{dataset}'


@click.command()
@click.option('--dataset', '-d', type=click.Choice(['fruit', 'car']), default='car')
@click.option('--no_of_objects', '-n', type=int, required=True, default=2)
@click.option('--no_of_experiments', '-e', type=int, default=1)
def main(dataset: str, no_of_objects: int, no_of_experiments: int):
    data_directory = BASE_IMAGE_PATH.format(dataset=dataset)

    if not isdir(data_directory):
        raise ValueError('Invalid data path given')

    model_trainer = ModelTraining()
    model_trainer.setModelTypeAsResNet()
    model_trainer.setDataDirectory(data_directory)
    model_trainer.trainModel(num_objects=no_of_objects,
                             num_experiments=no_of_experiments,
                             enhance_data=True,
                             batch_size=16,
                             show_network_summary=True)


if __name__ == '__main__':
    main()
