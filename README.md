# Machine Learning Assignment 3

The purpose of this assignment is to compare traditional machine learning approaches with deep learning 
approach for image classification tasks.
Full assignment description is found under `docs/AssignmentDescription.pdf`. 

## Setup


This project uses Python 3.7 & pip package installer.

Install the needed requirements:

```bash
pip install -r requirements.txt
```

## Traditional machine learning approach

_TODO_: Gent Rexha might want to write something here

## Deep learning approach


As basis for deep learning approach, [ImageAI](https://github.com/OlafenwaMoses/ImageAI) python library was used.
This libraryis build on top of tensorflow object detection API, and uses ResNET neural networks (which are 
the state of art in image classification tasks).

In order to enrich the dataset more - and supposedly the model efficiency - the following 
steps have been performed:  
  
  * rotating images (left, right and 180 degrees)

### Rotate Images

In order to learn the model better, its a common practice to enrich the dataset with 
rotated images so that it neural network sees things in different perspectives.

To do this run the following:

```bash
python src/helpers/rotate_images.py
```

This will store all images in the same path with `right` or `left` postfix.

### Training the model

Since ImageAI library was used, we have trained the CustomImagePrediction model (which uses ResNERZS as 
network model type).

To train the model, run the following:

```bash
cd src/models
python imageai_build_model.py --dataset car --no_of_objects 2 --no_of_experiments 100 
```

This may take different running times, depending on the number of training images, epochs, batch sizes etc.


**Note:** Please make sure that the folder structure for images is as following (e.g. for car):

* car
    * train
        * car
            * car-train-images
        * no-car
            * no-car-train-images
    * test
        * car
            * car-test-images
        * no-car
            * no-car-test-images
            
 After model is trained, the corresponding trained model is stored in `h5` format under 
 the `car/models/model_name.h5`

_TODO_: ILIR has to fix the part below here

### Run predictions on the model

After having the model trained, you can run predictions on it.
To run predictions on an image, run the following:

```bash
python kermit_model_evaluation.py -f [file path to image 
```

E.g. Predicting an image:

```bash
python kermit_model_evaluation.py -t image -f kermit.jpeg

Predicting the kermit.jpeg image
 kermit: 99.87 no-kermit: 0.13

```
...which is awesome.

Same can be done for a video:

```bash
python kermit_model_evaluation.py -t video -f MuppetsEpisode3.avi
```

This will get all the frames in 1 second interval from the video, store them under `tmp` (for now 
named as episode3_results) folder as jpegs with a banner on top of the image that shows the prediction result 
for each frame. 

## Example run:

Since episode3 was used as validation set, the results and corresponding predictions for it 
are stored under `episode3_results` directory. Check it out or see a part of classification

![Episode3 Results](https://github.com/ilirosmanaj/detect_kermit/blob/master/readme_images/episode3.gif)


As the gif shows, sometimes the model mixes Kermit like characters (green frogs) as Kermit, but not for 
the cases where there is no clear similarity.


Useful Links
------------
* https://github.com/tuwien-musicir/DL_Tutorial/blob/master/Car_recognition.ipynb
* http://cogcomp.org/Data/Car/


Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

## Troubleshooting:

pip distribution of tensorflow does not work for osX, so it can be installed via the url
using the following command:

```
python3 -m pip install --upgrade https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-1.12.0-py3-none-any.whl
```

Sometimes tensorflow gpu might complain about limited number of running devices. 
To fix it, just set the `CUDA_VISIBLE_DEVICES` environment variable as follows:

```
export CUDA_VISIBLE_DEVICES=''
```
